import pandas as pd
import numpy as np

# 1 DU = 2.7 e16

vcd = {'O3':1e19,      # ~400 DU
       'NO2':1e16,     # 3 - 30 e15
       'SO2':1e16,     #
       'H2O':1e22,     # dixit Alain et Elodie
       'BrO':5e13,     # 1 - 10 e13 
       'O4':1e43,      #
       'tau':0.2,
       'O2':1e60,
       'CHOCHO':4e14,
       'H2CO':6e15,
       'P': 0.91}

##########################################################

def smoother(df,signal,**kwargs):
    """
    calculates a smart smoothing of some coloumn in a df
    parameters:
    ----------
        df : DataFrame incoming
        signal : str, coloumn to smooth
    keywords:
    --------
        step : wavelengh step to use for various calculations
        method : defines the method to use; see rolling...
                'boxcar', 'gaussian' are dealt with
        resolution : resolution at which to smooth
    outputs:
    -------
        smoothed series, named `data`
    """
            
    step = df['wvl'].diff().min() #nm
    res = 1 #nm
    method = 'gaussian'
    inplace = 'False'
    
    if 'resolution' in kwargs:
        res = kwargs['resolution']    
    if 'method' in kwargs:
        method = kwargs['method']
    if 'inplace' in kwargs:
        inplace = kwargs['inplace']
        
    smooth='_'.join([signal,method,str(res),'nm'])
    
    if method == 'boxcar':
        ser = df[signal].rolling(int(res/step),
                                 center=True) \
                        .mean()
    if method =='triang':
        ser = df[signal].rolling(int(res/step),
                                 center=True,
                                 win_type='triang') \
                        .mean()
    if method == 'gaussian':
        std = max(int(res/step/6),1)
        ser = df[signal].rolling(int(res/step), 
                                 center=True, 
                                 win_type='gaussian') \
                        .mean(std=std)
    if method == 'TF':
        ser = df[['wvl','irradiance']].set_index('wvl').squeeze()
        ser = FT_smoother(ser)

    ser.name='data'
    ser.index=df['wvl']
    return ser


##############################################################################

def regular_resample(df, data='data'):
    """
    calculates a regularly resampled 
    """
    wvl = np.arange(df['wvl'].min(), df['wvl'].max(), df['wvl'].diff().min())
    df = pd.DataFrame({'wvl':wvl,
                       'data': np.interp(wvl, df['wvl'], df[data])})
    return df

###############################################################################



def initialise_Xsect (wvl, my_res=1, path = '../Xsect/'):
    """
    initialise_Xsect (wvl, my_res=1)
    gets all the cross sections from ../Xsect/,
    smooths at my_res
    casts over wvl
    returns a nice df
    
    can be long...
    """  
    
    
    O3 = pd.read_csv(path+"seo3_burrows293.data", 
                 skiprows=7, 
                 skipinitialspace=True, 
                 delim_whitespace=True, 
                 header=None, 
                 names = ['wvl','data'], 
                 index_col=False)
    O3.dropna(axis = 'rows', inplace = True)
    O3_lisse = smoother(regular_resample(O3),'data',resolution=my_res)  

    NO2 = pd.read_csv(path+'seno2_burrows293.data', 
                  skiprows=7, skipinitialspace=True, 
                  delim_whitespace=True, header=None, 
                  names = ['wvl','data'], index_col=False)
    NO2.dropna(axis = 0, inplace = True)
    NO2_lisse = smoother(regular_resample(NO2),'data',resolution=my_res)  

    SO2 = pd.read_csv(path+'Xsect_SO2-1867.txt', 
                  skiprows=7, skipinitialspace=True, 
                  delim_whitespace=True, header=None, 
                  names = ['wn','data'], index_col=False)
    SO2.dropna(axis = 0, inplace = True)
    SO2['wvl'] = 10**7/(SO2['wn']) # Conversion cm^-1 to nm
    SO2.sort_values(by='wvl',inplace=True)   
    SO2_lisse = smoother(regular_resample(SO2),'data',resolution=my_res)  
    
    H2O = pd.read_csv(path+'SpectrMol_H2O_Roberto.txt', 
                  skiprows=7, skipinitialspace=True, 
                  delim_whitespace=True, header=None, 
                  names = ['wn','data', 'absorb'], index_col=False)
    H2O.dropna(axis = 0, inplace = True)
    H2O['wvl'] = 10**7/(H2O['wn'])
    H2O.sort_values(by='wvl',inplace=True)  
    H2O_lisse = smoother(regular_resample(H2O),'data',resolution=my_res)  

    O4 = pd.read_csv(path+'Xsect_O2-O2-1947.txt', 
                 skiprows=4, skipinitialspace=True, 
                 delim_whitespace=True, header=None, 
                 names = ['wn','data'], index_col=False)
    O4.dropna(axis = 0, inplace = True)
    O4['wvl'] = 10**7/(O4['wn'])
    O4.sort_values(by='wvl',inplace=True)  
    O4_lisse = smoother(regular_resample(O4),'data',resolution=my_res)  

    O2 = pd.read_csv(path+'Xsect_O2-O2-1946.txt', 
                 skiprows=4, skipinitialspace=True, 
                 delim_whitespace=True, header=None, 
                 names = ['wn','data'], index_col=False)
    O2.dropna(axis = 0, inplace = True)
    O2['wvl'] = 10**7/(O2['wn'])
    O2.sort_values(by='wvl',inplace=True) 
    O2_lisse = smoother(regular_resample(O2),'data',resolution=my_res)  

    H2CO = pd.read_csv(path+'Xsect_H2CO-1835.txt', 
                   skiprows=4, skipinitialspace=True, 
                   delim_whitespace=True, header=None, 
                   names = ['wn','data'], index_col=False)
    H2CO.dropna(axis = 0, inplace = True)
    H2CO['wvl'] = 10**7/(H2CO['wn'])
    H2CO.sort_values(by='wvl',inplace=True)  
    H2CO_lisse = smoother(regular_resample(H2CO),'data',resolution=my_res)  

    BrO = pd.read_csv(path+'Xsect_BrO-1825.txt', 
                  skiprows=4, skipinitialspace=True, 
                  delim_whitespace=True, header=None, 
                  names = ['wn','data'], index_col=False)
    BrO.dropna(axis = 0, inplace = True)
    BrO['wvl'] = 10**7/(BrO['wn'])
    BrO.sort_values(by='wvl',inplace=True)  
    BrO_lisse = smoother(regular_resample(BrO),'data',resolution=my_res)  
    
    """CHOCHO = pd.read_csv('../Xsect/glyoxal_0.1nmFWHM_IUPbremen.TXT',
            skiprows=46,
            decimal=',', delim_whitespace=True,
            names=['lambdaAir', 'lambdaVac', 'wn', 'data'])
    CHOCHO.dropna(axis = 0, inplace = True)
    CHOCHO['wvl'] = 10**7/(CHOCHO['wn'])
    CHOCHO.sort_values(by='wvl',inplace=True)  
    CHOCHO_lisse = smoother(regular_resample(CHOCHO),'data',resolution=my_res)  
"""
    df_com = pd.DataFrame(index = wvl,
                          data = {'O3': np.interp(wvl,O3_lisse.index,O3_lisse.values),
                                  'NO2': np.interp(wvl,NO2_lisse.index,NO2_lisse.values),
                                  'SO2': np.interp(wvl,SO2_lisse.index,SO2_lisse.values),
                                  'H2O': np.interp(wvl,H2O_lisse.index,H2O_lisse.values),
                                  'O4': np.interp(wvl,O4_lisse.index,O4_lisse.values),
                                  'O2': np.interp(wvl,O2_lisse.index,O2_lisse.values),
                                  'H2CO': np.interp(wvl,H2CO_lisse.index,H2CO_lisse.values),
                                  'BrO': np.interp(wvl,BrO_lisse.index,BrO_lisse.values)})
    return df_com


def initialise_solar (wvl, my_res=1,path_solar = "../solarISS/"):
    """
    initialise_solar (wvl, my_res=1)
    gets ISS solar spectrum from /solarISS/,
    smooths at my_res
    casts over wvl
    returns a nice df
    """
    
    solar = pd.read_csv(path_solar+"SOLAR_ISS_V1.txt", 
                    delim_whitespace=True, 
                 skiprows=16, names=['wvl','irradiance','error'], 
                    index_col=False)
    solar.dropna(axis = 0, inplace = True)
    solar = solar[solar['wvl']<2200]
    solar_lisse = smoother(solar,'irradiance',resolution=my_res) 
    solar_com = pd.DataFrame({'wvl':wvl,
                              'solar':np.interp(wvl,
                                                solar_lisse.index,
                                                solar_lisse.values)}) \
                  .set_index('wvl')
    return solar_com

def diffusion_Molec(l):
    """
    calculates molecular diffusion scattering for 1atm as a function of wvl (nm)
    """
    return 449153487.24/(l)**4 


def tau(lamb, lamb_ref = 532, alpha = 1.22):
    """
    aerosol scattering, as a function of wavelength
    params:
        lamb: wavelength
        lamb_ref: reference wavelength (default value 532nm)
        alpha: angstrom exponent (default value 1.22)
    """
    return (lamb_ref/lamb)**alpha

def init_simul_(wvl, my_res=1):
    Xsect = initialise_Xsect(wvl,my_res)
    solar = initialise_solar(wvl,my_res)
    df = pd.concat([solar,Xsect],axis='columns')
    df['tau']=tau(df.index)
    df['P']=diffusion_Molec(df.index)
    return df
    

def init_simul(debut, fin ,my_res=1):
    """
    init_simul(debut, fin ,my_res=1)
    
    prepares a dataFrame for wavelengths from `debut` to `fin`
    at a given resolution (default = 1)
    """
    wvl = np.arange(debut,fin,my_res/10)
    
    return init_simul_(wvl, my_res)

def amf_straight(SZA):
    """
    from SZA as input, calculates a direct solar observation air mass factor
    """
    SZA = SZA/180*np.pi
    return 1/(1-np.sin(SZA)**2)**0.5

def simulation(df,cd,AMF):
    '''Cette fonction permet de simuler le spectre solaire atteignant le sol.
    I0 = spectre extrasolaire
    T = Combinaison lineaire des conc et des xsect
    I = Intensité solaire qui arrive au sol'''
    dff = df[['solar']].copy()
    dff.rename(columns ={'solar':'I0_solar'}, inplace=True)

    T = pd.concat([ df[gas]*cd[gas] for gas in cd ], axis='columns') \
          .sum(axis='columns')

    dff['I_sol'] = dff['I0_solar'] * np.exp(- AMF * T)
    return dff